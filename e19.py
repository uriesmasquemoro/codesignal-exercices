import re

def newStyleFormatting(s):
    s = re.sub('%%', '{%}', s)
    s = re.sub('%[Xxdfgeocbs]', '{}', s)
    return re.sub('{%}', '%', s)

